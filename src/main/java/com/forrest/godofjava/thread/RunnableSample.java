package com.forrest.godofjava.thread;

/**
 * Created by chgm1006 on 2015. 4. 14..
 */
public class RunnableSample implements Runnable {
  public void run() {
    System.out.println("This is RunnableSample's run() method.");
  }
}
