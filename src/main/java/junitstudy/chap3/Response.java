package junitstudy.chap3;

public interface Response {
  String getName();
}
